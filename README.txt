
Description
-----------
This module defines the "slider jQuery UI" form element.


Installation
------------
1) Place this module directory in your "modules" folder (this will usually be
"sites/all/modules/"). Don't install your module in Drupal core's "modules"
folder, since that will cause problems and is bad practice in general. If
"sites/all/modules" doesn't exist yet, just create it.

2) Enable the Slider of jQuery UI module.

Sample usage
--------------

<?php
  $form['slider'] = array(
    '#type' => 'ww_slider',
    '#title' => t('My slider'),
    '#format' => 'formatInterval' // You may utilite formatString, formatPlural
    '#format_arguments' => array(3) // Arguments of methods of format
    '#settings' => array(
      'range' => TRUE,
      'min' => 0,
      'max' => 86400,
      'values' => array(120, 3600),
      'step' => 60,
    ),
    '#attributes' => array(
      'readonly' => 'readonly',
    ),
);
?>
  * http://jqueryui.com/slider/
  * http://api.jqueryui.com/slider/

Author
------
Freen Jimenez Torres
email: freenjt@gmail.com

The author can be contacted to the developments concerning the module.

