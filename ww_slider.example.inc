<?php

/**
 * @file
 * Here it set a example to implement the slider control.
 * 
 * @see http://api.jqueryui.com/slider/
 */

/**
 * Form of example to slider.
 */
function ww_slider_example_form($form, &$form_state) {
  $form['slider'] = array(
    '#type' => 'ww_slider',
    '#title' => t('Vigence'),
    '#settings' => array(
      'range' => TRUE,
      'min' => 0,
      'max' => 100,
      'values' => array(40, 60),
      'step' => 2,
    ),
    '#attributes' => array(
      'readonly' => 'readonly',
    ),
    '#default_value' => t('@start to @end', array('@start' => 40, '@end' => 60)),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Send'),
  );
  return $form;
}

/**
 * Submit of form.
 */
function ww_slider_example_form_submit($form, &$form_state) {
  drupal_set_message(t('Range start = @start and Range end= @end', array(
    '@start' => $form_state['values']['slider']['range_start'],
    '@end' => $form_state['values']['slider']['range_end'],
  )));
}
