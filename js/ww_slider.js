(function ($) {
  // Here the slider is generated and its properties are set, according with the
  // jQuery UI APIs.
  Drupal.behaviors.wwSlider = {
    attach: function (context, settings) {
      $.each(Drupal.settings.wwSlider, function (key, value) {
        var nameSlider = "slider_" + value.id;
        var widthSlider = $('#' + value.id).width();

        // Creates the div where it will be the slider
        $('#' + value.id).after(
          '<div id="' + nameSlider + '" style="width:' + widthSlider + 'px;"></div>'
        );

        $('#' + value.id).once('ww-slider-procesed', function () {
          var rangeStart = "range_start_" + value.id;
          var rangeEnd = "range_end_" + value.id;

          // Add slider's captured value to the hidden fields
          $('#' + value.id).parent().append(
            '<input type="hidden" name="' + rangeStart + '" id="' + rangeStart + '" >'
          );
          $('#' + value.id).parent().append(
            '<input type="hidden" name="' + rangeEnd + '" id="' + rangeEnd + '" >'
          );

          var args = value.format_arguments.join();
          var args_format = value.format_arguments.length > 0 ? ',' + args : '';
          // Options by default
          var options = {
            slide: function (event, ui) {
              var val = value.format ? new Function(
                'return Drupal.' + value.format.concat('(' + ui.value + args_format + ')')
              )() : ui.value;

              $('#' + value.id).val(val);
              $('#' + rangeStart).val(ui.value);
            }
          };
          // Options with range
          if (value.settings.range) {
            options.slide = function (event, ui) {
              var val1 = ui.values[0];
              var val2 = ui.values[1];
              // If values will have format
              if (value.format) {
                var fun1 = new Function(
                  'return Drupal.' + value.format.concat('(' + ui.values[0] + args_format + ')')
                );
                var fun2 = new Function(
                  'return Drupal.' + value.format.concat('(' + ui.values[1] + args_format + ')')
                );
                val1 = fun1();
                val2 = fun2();
              }

              $('#' + value.id).val(val1 + ' ' + Drupal.t('to') + ' ' + val2);
              $('#' + rangeStart).val(ui.values[0]);
              $('#' + rangeEnd).val(ui.values[1]);
            };
          }
          var optionsVal = $.extend(true, options, value.settings);
          $('#' + nameSlider).slider(optionsVal);
        });
      });
    }
  };
})(jQuery);
